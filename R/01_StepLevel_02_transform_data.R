transform_data <- function(managed_data) {
    managed_data %<>% .choose_bp_for_tf_tracking()

    # TODO: Include phospho
    # TODO: include control in plot!
    managed_data %<>% normalize_data()

    # TODO: include phospho!
    # TODO: include control in plot!
    managed_data %<>% normalize_by_features()

    # TODO: include phospho!
    # TODO: include control in plot!
    managed_data %<>% impute_data()

    return(managed_data)
}

.choose_bp_for_tf_tracking <- function(
    managed_data) {

    protein_groups <- .batch_correct_otf(
        managed_data, data_set = "proteingroups")
    das <- .dimensions_available(
        protein_groups, "pca", min_variance_explained = 0)
    managed_data[["info"]][["pca_dims_guiding_transformations"]] <-
        if (length(das) >= 2) {c(1,2)} else {NULL}
    # dps <- .dimension_pairs(protein_groups, "pca",min_variance_explained = 0)
    #
    # dps_string <- dps %>%
    #     lapply(function(x) paste0("(", x[1], ", ", x[2], ")")) %>%
    #     unlist()
    # names(dps) <- dps_string
    # max_dim <- unlist(dps) %>% max(na.rm = TRUE)
    # choices_exist <- length(dps) > 1
    # if (choices_exist) {
    #     autonomics::biplot_covariates(
    #         object = protein_groups, covariates = "subgroup", ndim = max_dim,
    #         dimcols = length(dps))
    #     chosen_plotpair <- select_menue(dps_string, multiple_ok = FALSE,
    #         rowlimit = 1, default = 1,
    #         title = paste("Data transformations - Which biplot to use for",
    #             "guidance?"))
    # } else {
    #     cat("\nData transformations - Chosing the only available biplot for",
    #         "guidance (min. explained variance >= 5%).\n\n")
    #     chosen_plotpair <- 1
    # }
    # managed_data[["info"]][["pca_dims_guiding_transformations"]] <-
    #     magrittr::extract2(dps, chosen_plotpair)
    return(managed_data)
}

.add_dim_red <- function(
    x, pca = TRUE, pls = TRUE, pca_ndim = Inf, pls_ndim = 2) {
    if (pca) x %<>% autonomics::pca(ndim = pca_ndim, verbose = FALSE)
    if (pls & .more_than_one_subgroup(x)) x %<>%
        autonomics::pls(ndim = pls_ndim, verbose = FALSE)
    x
}

.dimensions_available <- function(
    object, method, min_variance_explained = 5, as_numeric = TRUE) {

    assertive::assert_is_a_bool(as_numeric)
    tmp_object <- do.call(
        utils::getFromNamespace(method, "autonomics"),
        list(
            object, ndim = Inf, minvar = min_variance_explained,
            verbose = FALSE))
    output <- autonomics::sdata(tmp_object) %>%
        names() %>%
        stringi::stri_subset_regex(paste0("^", method, ".*"))
    if (length(output) == 0) return(NULL)
    if (as_numeric) {
        output %<>%
            stringi::stri_extract_first_regex("\\d+$") %>%
            as.numeric()
    }
    variance_explained <- S4Vectors::metadata(tmp_object)[[method]]
    output %<>% magrittr::extract(variance_explained >= min_variance_explained)
    attr(output, "variance_explained") <- variance_explained[
        variance_explained >= min_variance_explained]
    return(output)
}

.dimension_pairs <- function(
    object, method, min_variance_explained = 5, as_numeric = TRUE) {

    assertive::assert_is_all_of(object, "SummarizedExperiment")
    method <- match.arg(method, c("pca", "pls", "lda", "sma"), FALSE)
    assertive::assert_is_a_number(min_variance_explained)
    assertive::assert_all_are_in_closed_range(min_variance_explained, 0, 100)
    assertive::assert_is_a_bool(as_numeric)

    dimensions <- .dimensions_available(
        object, method, min_variance_explained, as_numeric = as_numeric)
    if (is.null(dimensions)) return(NULL)
    dl <- length(dimensions)
    dp <- split(dimensions, ceiling(seq_along(dimensions) / 2))
    pl <- length(dp)
    if (dl > 2 && dl %% 2 == 1) dp[[pl]] <- NULL
    dp
}
