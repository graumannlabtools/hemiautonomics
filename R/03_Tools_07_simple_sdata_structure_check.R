simple_sdata_structure_check <- function(managed_data) {
    tibble::view(
        title = "sdata check: proteingroups",
        autonomics::sdata(managed_data$data$proteingroups) %>%
            .discard_dimension_reduction())
    structure_check <- select_menue(
        c("yes", "no"), title = c("Is data structure correct?"),
        multiple_ok = FALSE, rowlimit = 1) %>% magrittr::equals("yes")
    if (!structure_check)
        .stop_quietly("Data structure/naming requires manual intervention.")

    if (managed_data$info$analyze_phospho &&
            !.sdata_all_identical(managed_data$data))
        stop(
            "Incongruent data structure protein groups/phospho ",
            "sites/occupancies!?")

    return(managed_data)
}

.discard_dimension_reduction <- function(x) {
    x %>% dplyr::select(
        - dplyr::matches("^lda\\d+$"),
        - dplyr::matches("^pca\\d+$"),
        - dplyr::matches("^pls\\d+$"),
        - dplyr::matches("^sma\\d+$"))
}

.sdata_all_identical <- function(selist) {
    . <- sample_id <- NULL

    selist %>%
        lapply(autonomics::sdata) %>%
        lapply(dplyr::arrange, sample_id) %>%
        lapply(.discard_dimension_reduction) %>%
        sapply(FUN = identical, magrittr::extract2(., 1)) %>%
        all()
}