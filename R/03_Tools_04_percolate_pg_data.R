percolate_pg_data <- function(
    site_data,
    protein_groups,
    ...,
    aggregate_logicals = TRUE,
    sep = ";") {
    feature_id <- `Protein group IDs` <- NULL

    assertive::assert_is_all_of(site_data, "SummarizedExperiment")
    assertive::assert_is_subset(
        "Protein group IDs", autonomics::fvars(site_data))
    assertive::assert_is_all_of(protein_groups, "SummarizedExperiment")
    assertive::assert_is_subset("feature_id", autonomics::fvars(protein_groups))
    assertive::assert_is_a_bool(aggregate_logicals)
    assertive::assert_is_a_string(sep)

    # sites: Isolate matching data and separate
    rel_site_fdata <- autonomics::fdata(site_data) %>%
        dplyr::select(feature_id, `Protein group IDs`) %>%
        tidyr::separate_rows(`Protein group IDs`, sep = ";")

    # protein groups: extract relevant data
    pg_fdata <- autonomics::fdata(protein_groups) %>%
        dplyr::select(feature_id, ...)

    # Discard references to filtered out protein groups
    rel_site_fdata %<>%
        dplyr::filter(`Protein group IDs` %in% pg_fdata$feature_id)

    # Merge subsets
    rel_site_fdata %<>% dplyr::left_join(
        pg_fdata, by = c("Protein group IDs" = "feature_id")) %>%
        dplyr::select(-`Protein group IDs`)

    # Recompact
    if (aggregate_logicals) {
        rel_site_fdata_lgl <- rel_site_fdata %>%
            dplyr::select(
                feature_id, tidyselect:::where(function(x)is.logical(x)))
        rel_site_fdata %<>% dplyr::select(
            feature_id, tidyselect:::where(function(x)!is.logical(x)))
    }
    rel_site_fdata %<>%
        dplyr::group_by(feature_id) %>%
        dplyr::mutate_at(
            dplyr::vars(-dplyr::group_cols()),
            function(x) paste0(as.character(x), collapse = sep)) %>%
        dplyr::distinct(feature_id, .keep_all = TRUE)
    if (aggregate_logicals)
        rel_site_fdata %<>% dplyr::left_join(
            .logical_rel_site_fdata(rel_site_fdata_lgl, feature_id, ...),
            by = "feature_id")

    # Merge result back into original & return
    autonomics::fdata(site_data) %<>%
        dplyr::left_join(rel_site_fdata, by = "feature_id")
    return(site_data)
}

.logical_rel_site_fdata <- function(rel_site_fdata, feature_id, ...) {
    dplyr::select(rel_site_fdata, feature_id, ...) %>%
        dplyr::select(feature_id, tidyselect:::where(is.logical)) %>%
        dplyr::group_by(feature_id) %>%
        dplyr::mutate_at(dplyr::vars(-dplyr::group_cols()), any) %>%
        dplyr::distinct(feature_id, .keep_all = TRUE)
}