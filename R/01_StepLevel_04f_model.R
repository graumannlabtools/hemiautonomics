model <- function(managed_data) {
    for (nm in names(managed_data$data)) {
        if (stringi::stri_startswith_fixed(nm, "phospho")) {
            weightvar <- NULL
        } else {
            weightvar <- managed_data$processing$linear_modeling$weights
        }

        intercept_contrasts <- .intercept_contrast(managed_data, nm)
        if (all(intercept_contrasts)) {
            contrastdefs <- autonomics:::contrast_coefs(
                managed_data$data[[nm]],
                managed_data$processing$linear_modeling$formula)
        } else if (any(intercept_contrasts)) {
            stop("Mix of `Intercept` contrast(s) with others currently ",
                "unsuported")
        } else {
            contrastdefs <- managed_data$processing$linear_modeling$contrasts
        }

        message("\t", nm)
        managed_data$data[[nm]] %<>% autonomics::analyze(
            pca = FALSE,
            fit = managed_data$processing$linear_modeling$engine,
            subgroupvar = "subgroup",
            formula = managed_data$processing$linear_modeling$formula,
            block = managed_data$processing$linear_modeling$block,
            weightvar = weightvar,
            contrastdefs = contrastdefs,
            verbose = TRUE,
            plot = FALSE)
    }
    return(managed_data)
}

.intercept_contrast <- function(managed_data, nm) {
    autonomics:::contrast_coefs(
        managed_data$data[[nm]],
        managed_data$processing$linear_modeling$formula) %>%
        unlist() %>%
        stringi::stri_detect_regex("^[Ii]ntercept$") %>%
        stats::na.omit()
}