read_data <- function(managed_data) {
    managed_data %<>% detect_existing_mq_files()

    managed_data %<>% import_mq_data()

    # Sample-level info
    managed_data %<>% simple_sdata_structure_check()

    managed_data %<>% augment_sdata()

    # managed_data %<>% evaluate_covariate_correlation()

    # Feature-level info
    managed_data %<>% evaluate_feature_count()

    managed_data %<>% id_control_features()

    managed_data %<>% simplify_proteingroups()

    return(managed_data)
}
